#pragma once
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

char* f(char* s, int n) {
	if (n <= 0) return "";
	
    int newLength = 0;
    for (int i = 0; i < n; i++) {
        if (s[i] == '&') newLength += 3;
        if (s[i] == '<' || s[i] == '>')  newLength += 2;
        newLength++;
    }
    if (newLength <= 0) return "";
    
    char ret[newLength + 1];
    int k = 0;
    for (int i = 0; i < n; i++) {
        if (s[i] == '&') {
            ret[k] = '&'; ret[k+1] = 'a'; ret[k+2] = 'm'; ret[k+3] = 'p';
            k += 4;
        }  else if (s[i] == '<') {
            ret[k] = '&'; ret[k+1] = 'l'; ret[k+2] = 't';
            k += 3;
        } else if (s[i] == '>') {
            ret[k] = '&'; ret[k+1] = 'g'; ret[k+2] = 't';
            k += 3;
        } else {
            ret[k] = s[i];
            k++;
        }
    }
    ret[k] = '\0';
    return ret;
}
