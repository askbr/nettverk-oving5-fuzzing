# Øving 5

Gitlab link til prosjektet: https://gitlab.stud.idi.ntnu.no/askbr/nettverk-oving5-fuzzing

*Note: prosjektet er basert på eksempelet https://gitlab.com/ntnu-tdat3020/fuzzing-example*

## Feil oppdaget

### Address Sanitizer

Address sanitizer oppdaget en feil i funksjonen fra øving 4: **"heap-buffer-overflow"**.

Denne ble fikset ved å legge til `\0` på slutten av en string, noe jeg hadde glemt. 

### Fuzzing

En feil funnet  i funksjonen fra øving 4 (spesifikt `char* f(char* s, int n)` i utility.h) gjennom fuzzing var **"memory leak"**. 

Denne ble fikset ved å istedenfor å opprette stringen som returneres fra funksjonen på heap allokeres den på stack. 

Altså `char ret[n];` framfor `char* ret = malloc(n);`.

## CI

Siste kjøring av CI kan finnes gjennom lenken: 

https://gitlab.stud.idi.ntnu.no/askbr/nettverk-oving5-fuzzing/-/pipelines/186364
