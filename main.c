#include "utility.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
	char* str1 = "test";
	char* str2 = "test&test<test>";
	char* test1 = f(str1, 4);
	printf("Input #1 \"test\", Output: %s\n", test1);
	char* test2 = f(str2, 15);
	printf("Input #2 \"test&test<test>\", Output: %s\n", test2);
	return 0;
}
